package com.example.mvcproject.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.mvcproject.Pojo.MovieModel;
import com.example.mvcproject.R;

public class MainActivity extends AppCompatActivity {

    /* MVC -> (Model View Controller) Architecture Design Pattern *
    * Model -> Some Data From DB
    * View -> Layout XML
    * Controller -> Activity
    * */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView textView = findViewById(R.id.textView);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(getMovieName().getName());
            }
        });
    }

    public MovieModel getMovieName() {
        return new MovieModel("Nasser Movie","1");
    }
}
